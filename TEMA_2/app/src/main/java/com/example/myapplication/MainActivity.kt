package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.example.myapplication.adapters.MyAdapter
class MainActivity : AppCompatActivity() {
   val adapter: MyAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpRecyclerView()
        getPosts()
        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener{
            createNewUser()
        }
        fun setUpRecyclerView(){
            val recyclerView:RecyclerView=findViewById(R.id.list)
            val layoutManager: LinearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        }

    }

    private fun createNewUser() {
        TODO("Not yet implemented")
    }

    private fun getPosts() {
        TODO("Not yet implemented")
    }

    private fun setUpRecyclerView() {
        TODO("Not yet implemented")
    }
}