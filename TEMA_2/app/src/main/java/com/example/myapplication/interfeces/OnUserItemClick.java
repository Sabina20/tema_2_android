package com.example.myapplication.interfeces;

import com.example.myapplication.models.User;

public interface OnUserItemClick {
    void onClick(User user);

}
