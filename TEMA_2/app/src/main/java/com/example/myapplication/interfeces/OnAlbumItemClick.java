package com.example.myapplication.interfeces;

import com.example.myapplication.models.Album;
public interface OnAlbumItemClick {
    void onClick(Album album);
}
