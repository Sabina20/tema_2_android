package com.example.myapplication.adapters;

import com.example.myapplication.R;
import com.example.myapplication.interfeces.OnAlbumItemClick;
import com.example.myapplication.models.SocialMedia;
import com.example.myapplication.models.SocialMediaType;
import com.example.myapplication.models.User;
import com.example.myapplication.models.Album;
import com.example.myapplication.models.Post;
import com.example.myapplication.interfeces.OnUserItemClick;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<SocialMedia> socialMediaList;
    OnUserItemClick onUserItemClick;
    OnAlbumItemClick onAlbumItemClick;

    @Override
    public int getItemViewType(int position){
        if(socialMediaList.get(position).getType()== SocialMediaType.USER){
             return 0;
        }else if(socialMediaList.get(position).getType()==SocialMediaType.ALBUM){
            return 1;
        }
        return super.getItemViewType(position);
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,int viewType){
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        if(viewType==0){
            View view=inflater.inflate(R.layout.item_cell,parent,false);
            UserViewHolder userViewHolder= new UserViewHolder(view);
            return userViewHolder;
        }
        return null;

    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder,int position){
        if(holder instanceof UserViewHolder){
            User user=(User)socialMediaList.get(position);
            ((UserViewHolder)holder).bind(user);
        }else if(holder instanceof AlbumHolder){
            Album album=(Album)socialMediaList.get(position);
            ((AlbumHolder)holder).bind(album);
        }
    }
    @Override
    public int getItemCount(){ return this.socialMediaList.size();}
    class UserViewHolder extends RecyclerView.ViewHolder{
        private TextView name;
        private TextView userPostsList;
        private View view;

        UserViewHolder(View view){
            super(view);
            name=view.findViewById(R.id.name);
            userPostsList=view.findViewById((R.id.userPostsList));
            this.view=view;
        }
        void bind(User user){
            name.setText(user.getName());
            userPostsList.setText(user.getUserPostsList());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onUserItemClick !=null){
                        onUserItemClick.onClick(user);
                    }
                }
            });
        }
    }
    class AlbumHolder extends RecyclerView.ViewHolder{
        private TextView label;
        private View view;
        AlbumHolder(View view){
            super(view);
            label=view.findViewById(R.id.label);
            this.view=view;
        }
        void bind(Album album){
            label.setText(album.getLabel());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onAlbumItemClick !=null){
                        onAlbumItemClick.onClick(album);
                    }
                }
            });
        }
    }
}
