package com.example.myapplication.models;

public class Album extends SocialMedia {
    private String label;

    public Album(String label){
        super(SocialMediaType.ALBUM);
        this.label=label;
    }
    public String getLabel(){ return label;}

    public void setLabel(String label) { this.label=label;}
}
