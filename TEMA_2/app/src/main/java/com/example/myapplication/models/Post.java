package com.example.myapplication.models;

public class Post extends SocialMedia{
    private String description;

    public String getDescription(){ return description;}
    public void setDescription(String description){ this.description=description;}

    public Post(String description){
        super(SocialMediaType.POST);
        this.description=description;
    }
}
