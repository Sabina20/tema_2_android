package com.example.myapplication.models;

import java.util.ArrayList;

public class User extends SocialMedia{
   private String name;
   private ArrayList<Post>  userPostsList;

   public void setname(String name){ this.name=name;}
   public void setUserPostsList(ArrayList<Post>  userPostsList){this.userPostsList=userPostsList;}
   public String getName(){ return name;}
   public CharSequence getUserPostsList(){ return (CharSequence) userPostsList;}
   public User(String name,ArrayList<Post> userPostsList){
       super(SocialMediaType.USER);
       this.name=name;
       this.userPostsList=userPostsList;
   }
}
