package com.example.myapplication;

import android.app.Application;

public class AplicationController extends Application {
    private static AplicationController instance;

    @Override
    public void onCreate(){
        super.onCreate();
        instance=this;
    }
    public static AplicationController getInstance(){ return instance;}
}
