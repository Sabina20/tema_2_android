package com.example.myapplication.models;

public class SocialMedia {
    public SocialMediaType getType(){ return type;}

    public void setType(SocialMediaType type) { this.type=type;}

    SocialMediaType type;
    SocialMedia(SocialMediaType type){ this.type=type;}
}
