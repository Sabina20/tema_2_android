package com.example.myapplication.models;

public enum SocialMediaType {
    USER,
    ALBUM,
    POST
}
